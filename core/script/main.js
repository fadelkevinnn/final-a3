/* Ini adalah file main.js yang akan menjadi tempat menulis javascript */


/* ##### Variables Pages Section ##### */
const homePages = document.querySelector("#homepage");
const quizPages = document.querySelector("#quizpage");
const finalPages = document.querySelector("#finalpage");


/* ##### Button Start Function ##### */
let mulaiBtn = document.querySelector("#mulai");

mulaiBtn.addEventListener("click", function(){
    mulaiQuiz();
})

let input = document.getElementById("text")

function mulaiQuiz() {
    let paragraph = document.getElementById("parag")

    if (input.value.length == 0) {
        paragraph.innerHTML = `<i class="bi bi-x-octagon-fill"></i> Error! name must not be empty.`;

    } else if (input.value.length < 4 ) {
        paragraph.innerHTML = `<i class="bi bi-x-octagon-fill"></i> Error! name must be longer than 3 characters.`;

    } else if  (input.value.length > 21 ) {
        paragraph.innerHTML = `<i class="bi bi-x-octagon-fill"></i> Error! name must be shorter than 20 characters.`;
    } 
    else {
        homePages.style.display = "none";
        quizPages.style.display = "block";
    }
}


/* ##### JSON Static Object ##### */
let quizData = [
    {   
        number: "1",
        question: "3*3?",
        answer: {
            a: "7",
            b: "5",
            c: "9"
            },
            correctAnswer: "c"
        },
    {
        number: "2",
        question: "4*5?",
        answer: {
            a: "10",
            b: "20",
            c: "15"
            },
            correctAnswer: "b"
        },
    {
        number: "3",
        question: "4*3?",
        answer: {
            a: "10",
            b: "11",
            c: "12"
            },
            correctAnswer: "c"
        },
    {
        number: "4",
        question: "5*5?",
        answer: {
            a: "20",
            b: "25",
            c: "10"
            },
            correctAnswer: "b"
        },
    {
        number: "5",
        question: "4*4?",
        answer: {
            a: "16",
            b: "20",
            c: "11"
            },
            correctAnswer: "a"
        },
    {
        number: "6",
        question: "7*2?",
        answer: {
            a: "11",
            b: "10",
            c: "14"
            },
            correctAnswer: "c"
        },
    {
        number: "7",
        question: "10*10?",
        answer: {
            a: "200",
            b: "50",
            c: "100"
            },
            correctAnswer: "c"
        },
    {
        number: "8",
        question: "2*3?",
        answer: {
            a: "3",
            b: "7",
            c: "5"
            },
            correctAnswer: "c"
        },
    {
        number: "9",
        question: "5*5?",
        answer: {
            a: "30",
            b: "25",
            c: "40"
            },
            correctAnswer: "b"
        },
    {
        number: "10",
        question: "100*100?",
        answer: {
            a: "500",
            b: "800",
            c: "10000"
            },
            correctAnswer: "c"
        },
];


/* ##### Looping Quiz ##### */
let quizPagesHTML = "";

quizData.forEach((array, num) => {
    if (num === 0){
        quizPagesHTML += `
        <div>
            <div class="question-page__number">
                <h3>${array.number}</h3>
            </div>
            <div>
                <h2>${array.question}</h2>
                <div>
                    <input type="radio" name="q-${array.number}" value="a" id="a${array.number}">
                    <label for="a${array.number}">${array.answer.a}</label>
                    <input type="radio" name="q-${array.number}" value="b" id="b${array.number}">
                    <label for="b${array.number}">${array.answer.b}</label>
                    <input type="radio" name="q-${array.number}" value="c" id="c${array.number}">  
                    <label for="c${array.number}">${array.answer.c}</label>
                </div>
                <div>
                    <button class="btn-next">Next</button>
                </div>
            </div>
        </div>`
    } else if (num <= 8 ){
        quizPagesHTML += `
        <div style="display:none">
            <div>
                <h3>${array.number}</h3>
                <h2>${array.question}</h2>
                <div>
                    <input type="radio" name="q-${array.number}" value="a" id="a${array.number}">
                    <label for="a${array.number}">${array.answer.a}</label>
                    <input type="radio" name="q-${array.number}" value="b" id="b${array.number}">
                    <label for="b${array.number}">${array.answer.b}</label>
                    <input type="radio" name="q-${array.number}" value="c" id="c${array.number}">  
                    <label for="c${array.number}">${array.answer.c}</label>
                </div>
                <div>
                    <button class="btn-prev">Prev</button>
                    <button class="btn-next">Next</button>
                </div>
            </div>
        </div>`
    } else {
        quizPagesHTML += `
        <div style="display:none">
            <div>
                <h3>${array.number}</h3>
                <h2>${array.question}</h2>
                <div>
                    <input type="radio" name="q-${array.number}" value="a" id="a${array.number}">
                    <label for="a${array.number}">${array.answer.a}</label>
                    <input type="radio" name="q-${array.number}" value="b" id="b${array.number}">
                    <label for="b${array.number}">${array.answer.b}</label>
                    <input type="radio" name="q-${array.number}" value="c" id="c${array.number}">  
                    <label for="c${array.number}">${array.answer.c}</label>
                </div>
                <div>
                    <button class="btn-prev">Prev</button>
                    <button class="btn-finish">Finish</button>
                </div>
            </div>
        </div>`
    }
});

quizPages.innerHTML = quizPagesHTML;



/* ##### Button Next Function ##### */
let btnNext = document.querySelectorAll(".btn-next");
btnNext.forEach((btn) => {
    btn.addEventListener("click", (e) => {
        
        let quizes = e.target.parentElement.parentElement.parentElement
        quizes.setAttribute("style", "display:none;")

        let quizesSibling = quizes.nextElementSibling
        quizesSibling.removeAttribute("style")
    })
})


/* ##### Button Previous Function ##### */
let btnPrev = document.querySelectorAll(".btn-prev");
btnPrev.forEach((btn) => {
    btn.addEventListener("click", (e) => {
        let quizes = e.target.parentElement.parentElement.parentElement
        quizes.setAttribute("style", "display:none;")

        let quizesSibling = quizes.previousElementSibling
        quizesSibling.removeAttribute("style")
    })
})


/* ##### Button Finish Function ##### */
let btnFinish = document.querySelector(".btn-finish");
btnFinish.addEventListener("click", function() {

    finish();
    finalAnswer();
});


function finish() {
    quizPages.style.display = "none";
    finalPages.style.display = "block";
}

/* ##### Looping Scoring ##### */
function finalAnswer() {
    let answered = []
    
    for (let i = 1; i <= 10; i++){  
        let userAnswered = document.querySelector(`input[name=q-${i}]:checked`).value;
            answered.push(userAnswered);
        }

    let score = 0;    
    answered.forEach((answers, num) =>{
        if(answers === quizData[num].correctAnswer){
            score +=1;
        }
        
    });

    correctAnswer(score);
}


/* ##### Result Section ##### */

function correctAnswer(score) {
    let resultHTML = "";

    if (score < 7) {
        resultHTML += `
        <h2>${input.value}</h2>
        <h3>Thanks, for your interest in my quiz!</h3>
        <h4>Result :</h4>
        <h5 class="first">${score} / 10</h5>
        <p class="first">You're not my friend's</p>
        <button class="result-page__again btn-again">Again!</button>`
    } else if (score < 10) {
        resultHTML += `
        <h2>${input.value}</h2>
        <h3>Thanks, for your interest in my quiz!</h3>
        <h4>Result :</h4>
        <h5 class="second">${score} / 10</h5>
        <p class="second">You're my friend's</p>
        <button class="result-page__again btn-again">Again!</button>`
    } else {
        resultHTML += `
        <h2>${input.value}</h2>
        <h3>Thanks, for your interest in my quiz!</h3>
        <h4>Result :</h4>
        <h5 class="third">${score} / 10</h5>
        <p class="third">You're my Best Friend's Forever!</p>
        <button class="result-page__again btn-again">Again!</button>`
    } 

    finalPages.innerHTML = resultHTML;


    let btnAgain = document.querySelector(".btn-again");

    btnAgain.addEventListener("click", function() {
        window.location.reload();
    });
};

// const checkbox = document.getElementById('checkbox');

// checkbox.addEventListener('change', ()=>{
//   document.body.classList.toggle('dark');
// })

function toggleDarkLight() {
    var body = document.getElementById("body");
    var currentClass = body.className;
    body.className = currentClass == "dark-mode" ? "light-mode" : "dark-mode";
  }



